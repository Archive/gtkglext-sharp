
using GdkGL;
using System;


namespace GtkGL
{
	public class GlUtilities 
	{
		static private int GetAttrib (Config glconfig, ConfigAttrib attribute)
		{
			int value;
			bool ret = glconfig.GetAttrib ((int)attribute, out value);
			return ret ? value : -1;
		}
		
		static public void WriteOutConfig (Config glconfig)
		{
		
		
/*
  g_print ("\n");
  g_print ("GL_RENDERER   = %s\n", (char *) glGetString (GL_RENDERER));
  g_print ("GL_VERSION    = %s\n", (char *) glGetString (GL_VERSION));
  g_print ("GL_VENDOR     = %s\n", (char *) glGetString (GL_VENDOR));
  g_print ("GL_EXTENSIONS = %s\n", (char *) glGetString (GL_EXTENSIONS));
  g_print ("\n");
*/


			Console.WriteLine ("OpenGL visual configurations :\n");
		    Console.WriteLine ("glconfig->is_rgba() = {0}", glconfig.IsRgba);
	  		Console.WriteLine ("glconfig->is_double_buffered() = {0}", glconfig.IsDoubleBuffered);
			Console.WriteLine ("glconfig->is_stereo() = {0}", glconfig.IsStereo);
			Console.WriteLine ("glconfig->has_alpha() = {0}", glconfig.HasAlpha);
			Console.WriteLine ("glconfig->has_depth_buffer() = {0}", glconfig.HasDepthBuffer);
			Console.WriteLine ("glconfig->has_stencil_buffer() = {0}", glconfig.HasStencilBuffer);
			Console.WriteLine ("glconfig->has_accum_buffer() = {0}", glconfig.HasAccumBuffer);
	
			Console.WriteLine ("Gdk::GL::USE_GL = {0}", GetAttrib (glconfig, ConfigAttrib.UseGl));
			Console.WriteLine ("Gdk::GL::BUFFER_SIZE = {0}", GetAttrib (glconfig, ConfigAttrib.BufferSize));     
			Console.WriteLine ("Gdk::GL::LEVEL = {0}", GetAttrib (glconfig, ConfigAttrib.Level));           
			Console.WriteLine ("Gdk::GL::RGBA = {0}", GetAttrib(glconfig, ConfigAttrib.Rgba));            
			Console.WriteLine ("Gdk::GL::DOUBLEBUFFER = {0}", GetAttrib(glconfig, ConfigAttrib.Doublebuffer));     
			Console.WriteLine ("Gdk::GL::STEREO = {0}", GetAttrib(glconfig, ConfigAttrib.Stereo));          
			Console.WriteLine ("Gdk::GL::AUX_BUFFERS = {0}", GetAttrib(glconfig, ConfigAttrib.AuxBuffers));      
			Console.WriteLine ("Gdk::GL::RED_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.RedSize));        
			Console.WriteLine ("Gdk::GL::GREEN_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.GreenSize));      
			Console.WriteLine ("Gdk::GL::BLUE_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.BlueSize));       
			Console.WriteLine ("Gdk::GL::ALPHA_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.AlphaSize));      
			Console.WriteLine ("Gdk::GL::DEPTH_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.DepthSize));      
			Console.WriteLine ("Gdk::GL::STENCIL_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.StencilSize));    
			Console.WriteLine ("Gdk::GL::ACCUM_RED_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.AccumRedSize));  
			Console.WriteLine ("Gdk::GL::ACCUM_GREEN_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.AccumGreenSize));
			Console.WriteLine ("Gdk::GL::ACCUM_BLUE_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.AccumBlueSize)); 
			Console.WriteLine ("Gdk::GL::ACCUM_ALPHA_SIZE = {0}", GetAttrib(glconfig, ConfigAttrib.AccumAlphaSize));
		}
	}
}

