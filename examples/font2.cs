/*
 * font.c:
 * Simple bitmap font rendering example.
 *
 * written by Naofumi Yasufuku  <naofumi@users.sourceforge.net>
 */
 
/*
 * font.cs:
 * Conversion for GTK# - gtkgl-sharp.
 *
 * converted by Luciano Martorella  <mad_lux_it@users.sourceforge.net>
*/

using Gtk;
using GtkSharp;
using GdkGL;
using GtkGL;
using System;
using Tao.OpenGl;

using Pango;



namespace Gtk 
{
	class	FontArea : DrawingArea
	{
		private	GlWidget m_gl = null;
		private bool m_realized = false;

		public FontArea (Config config)
		{
			m_gl = new GlWidget (this, config);
			Realized += new EventHandler (OnRealize);
			ConfigureEvent += new ConfigureEventHandler (OnConfigure);
			ExposeEvent += new ExposeEventHandler (OnExpose);
		}

		private string font_string = "courier 12";
		private int font_list_base;
		private int font_height;

		private static int PANGO_PIXELS (int d)
		{
			int PANGO_SCALE = 1024;
			if (d >= 0) 
				return (d + PANGO_SCALE / 2) / PANGO_SCALE;
			else
				return (d - PANGO_SCALE / 2) / PANGO_SCALE;
		}


		private void OnRealize (object o, EventArgs args)
		{
			// OpenGL BEGIN
			m_gl.MakeCurrent ();

			// Generate font display lists.
			font_list_base = Gl.glGenLists (128);
			FontDescription font_desc = FontDescription.FromString (font_string);
			
			Pango.Font font = GdkGL.Font.UsePangoFont (font_desc, 0, 128, font_list_base);
			if (font == null) {
			    Console.WriteLine ("*** Can't load font '{0}'", font_string);
			    throw new Exception ();
			}
			
			FontMetrics font_metrics = font.GetMetrics (Language.FromString ("italian"));
			
			font_height = font_metrics.Ascent + font_metrics.Descent;
			font_height = PANGO_PIXELS (font_height);
			
//			font_desc.Free ();
//			font_metrics.Unref ();
			
			Gl.glClearColor (1, 1, 1, 1);
			Gl.glClearDepth (1);
			
			Gl.glViewport (0, 0, Allocation.width, Allocation.height);
			
			Gl.glMatrixMode (Gl.GL_PROJECTION);
			Gl.glLoadIdentity ();
			Gl.glOrtho (0, Allocation.width, 0, Allocation.height, -1, 1);
			
			Gl.glMatrixMode (Gl.GL_MODELVIEW);
			Gl.glLoadIdentity ();
			
			// OpenGL END
			m_gl.SwapBuffers ();

			m_realized = true;
			QueueResize();
		}
		
		private void OnConfigure (object o, ConfigureEventArgs args)
		{
			if (!m_realized)
				return;

			// OpenGL BEGIN 
			m_gl.MakeCurrent ();
			
			Gl.glViewport (0, 0, Allocation.width, Allocation.height);
			Gl.glMatrixMode (Gl.GL_PROJECTION);
			Gl.glLoadIdentity ();
			Gl.glOrtho (0, Allocation.width,
			        	0, Allocation.height,
			        	-1, 1);
			Gl.glMatrixMode (Gl.GL_MODELVIEW);
			Gl.glLoadIdentity ();
		}

		private void OnExpose (object o, ExposeEventArgs args)
		{
			//// OpenGL BEGIN **
			m_gl.MakeCurrent ();

			Gl.glClear (Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
			
			// Draw some text.
			Gl.glColor3f (0, 0, 0);
			for (int i = 2; i >= -2; i--) {
			    Gl.glRasterPos2f (10.0f, (float)(0.5 * Allocation.height + i * font_height));
			    for (int j = ' '; j <= 'Z'; j++)
			    	Gl.glCallList (font_list_base + j);
			}
			
			// Show font description string.
			Gl.glColor3f (1, 0, 0);
			Gl.glRasterPos2f (10.0f, 10.0f);
			Gl.glListBase (font_list_base);
			Gl.glCallLists (font_string.Length, Gl.GL_UNSIGNED_BYTE, font_string);
			
			// OpenGL END **
			m_gl.SwapBuffers ();
		}
	}
}






class MainApp 
{
	private static void change (int[] arr)
	{
		arr[0] = 10;
	}
	private static int Main (string[] args)
	{
		int[] a = new int[1];
		a[0] = 2;
		Console.WriteLine ("{0}", a[0]);
		change (a);
		Console.WriteLine ("{0}", a[0]);
	
		// Init GTK.
		Gtk.Application.Init (ref args);
		
		// Init GtkGLExt.
		GtkGL.Application.Init (ref args);
		
		// Query OpenGL extension version.
		int major, minor;
		Query.Version (out major, out minor);
		Console.WriteLine ("\nOpenGL extension version - {0}.{1}", major, minor);
		
		// * Configure OpenGL-capable visual.
		// Try double-buffered visual 
		GdkGL.Config glconfig = new GdkGL.Config (GdkGL.ConfigMode.Rgb | GdkGL.ConfigMode.Double);
		if (glconfig == null) {
      		Console.WriteLine ("*** Cannot find the double-buffered visual.\n*** Trying single-buffered visual.");
			glconfig = new GdkGL.Config (GdkGL.ConfigMode.Rgb);
			if (glconfig == null) {
		          Console.WriteLine ("*** Cannot find any OpenGL-capable visual.");
		          return 1;
			} 
		}

		GlUtilities.WriteOutConfig (glconfig);
		
		// Top-level window.
		Window window = new Window (WindowType.Toplevel);
		window.Title = "font";
		
		// Get automatically redrawn if any of their children changed allocation. 
		window.ReallocateRedraws = true;
		window.DeleteEvent += new DeleteEventHandler (Window_Delete);
		
		// VBox.
		VBox vbox = new VBox (false, 0);
		window.Add (vbox);
		vbox.Show ();
		
		// Drawing area for drawing OpenGL scene.
		FontArea drawing_area = new FontArea (glconfig);
		drawing_area.SetSizeRequest (640, 240);
		
		vbox.PackStart (drawing_area, true, true, 0);
		drawing_area.Show ();
		
		// Simple quit button.
		Button button = new Button ("Quit");
		button.Clicked += new EventHandler (Button_Click);
		vbox.PackStart (button, false, false, 0);
		button.Show ();
		
		// Show window.
		window.Show ();		
		
		//
		Gtk.Application.Run ();
		return 0;
	}
		
	static void Button_Click (object obj, EventArgs args)
	{
		Gtk.Application.Quit ();
	}
		
	static void Window_Delete (object obj, DeleteEventArgs args)
	{
		Gtk.Application.Quit ();
	}

}

