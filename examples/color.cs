/*
 * color.c:
 * Color management example.
 *
 * written by Naofumi Yasufuku  <naofumi@users.sourceforge.net>
*/

/*
 * color.cs:
 * Conversion for GTK# - gtkgl-sharp.
 *
 * converted by Luciano Martorella  <mad_lux_it@users.sourceforge.net>
*/


using Gtk;
using GtkSharp;
using GdkGL;
using GtkGL;
using System;
using Tao.OpenGl;



// Colors.

namespace Gtk
{
	class	ColorTriangle : DrawingArea
	{
		private	GlWidget m_gl = null;
		private bool m_realized = false;
		
		public ColorTriangle (GdkGL.Config config)
		{			
			m_gl = new GtkGL.GlWidget (this, config);
			Realized += new EventHandler (OnRealize);
			ConfigureEvent += new ConfigureEventHandler (OnConfigure);
			ExposeEvent += new ExposeEventHandler (OnExpose);
		}

		private void OnRealize (object o, EventArgs args)
		{
			m_gl.MakeCurrent ();
			m_realized = true;
			QueueResize();
		}
		
		private void OnConfigure (object o, ConfigureEventArgs args)
		{
			if (!m_realized)
				return;
				
			//// OpenGL BEGIN **
			m_gl.MakeCurrent ();
			Gl.glViewport (0, 0, Allocation.Width, Allocation.Height);
			//// OpenGL END **
		}

		private void OnExpose (object o, ExposeEventArgs args)
		{
			//// OpenGL BEGIN **
			m_gl.MakeCurrent ();
		
			Gl.glClear (Gl.GL_COLOR_BUFFER_BIT);
			
			Gl.glBegin (Gl.GL_TRIANGLES);
			Gl.glIndexi ((int)ColorApp.RED.Pixel);
			Gl.glColor3f (1.0f, 0, 0);
			Gl.glVertex2i (0, 1);
			Gl.glIndexi ((int)ColorApp.GREEN.Pixel);
			Gl.glColor3f (0, 1.0f, 0);
			Gl.glVertex2i (-1, -1);
			Gl.glIndexi ((int)ColorApp.BLUE.Pixel);
			Gl.glColor3f (0, 0, 1.0f);
			Gl.glVertex2i (1, -1);  
			Gl.glEnd ();
			
			m_gl.SwapBuffers ();
	
			//// OpenGL END **
		}
	}	
} 




class	ColorApp
{
	public static readonly	Gdk.Color BLACK = new Gdk.Color (0x0, 0x0, 0x0); 		 
	public static readonly	Gdk.Color RED = new Gdk.Color   (0xff, 0x0, 0x0); 		 
	public static readonly	Gdk.Color GREEN = new Gdk.Color (0x0, 0xff, 0x0); 		 
	public static readonly	Gdk.Color BLUE = new Gdk.Color  (0x0, 0x0, 0xff); 		 
	
	private static  Gdk.Color[] colors = new Gdk.Color[] { BLACK, RED, GREEN, BLUE };



	private const int NUM_COLORS = 4;

	static private int Main (string[] args)
	{
		// * Init GTK.
		Gtk.Application.Init ("Color", ref args);
		
		// * Init GtkGLExt.
		GtkGL.Application.Init (ref args);	

		// * Display mode.
		GdkGL.ConfigMode mode = GdkGL.ConfigMode.Index;
		
		for (int i = 0; i < args.Length; i++)
		{
		    if (args[i] == "--rgb")
			    mode = GdkGL.ConfigMode.Rgb;
		}
		
		// * Query OpenGL extension version.
		int major, minor;
		Query.Version (out major, out minor);
		Console.WriteLine ("\nOpenGL extension version - {0}.{1}", major, minor);
		
		// * Configure OpenGL-capable visual.
		// Try double-buffered visual 
		GdkGL.Config glconfig = new GdkGL.Config (mode | GdkGL.ConfigMode.Depth | GdkGL.ConfigMode.Double);
		if (glconfig == null) {
      		Console.WriteLine ("*** Cannot find the double-buffered visual.\n*** Trying single-buffered visual.");
			glconfig = new GdkGL.Config (mode | GdkGL.ConfigMode.Depth);
			if (glconfig == null) {
		          Console.WriteLine ("*** Cannot find any OpenGL-capable visual.");
		          return 1;
			} 
		}

		GlUtilities.WriteOutConfig (glconfig);
		bool is_rgba = glconfig.IsRgba;
		
		// Top-level window.
		Window window = new Window (WindowType.Toplevel);
		window.Title = "color";

		// Perform the resizes immediately
		window.ResizeMode = ResizeMode.Immediate;
		
		// Get automatically redrawn if any of their children changed allocation.
		window.ReallocateRedraws = true;
		
		window.DeleteEvent += new DeleteEventHandler (Window_Delete);
		
		// VBox.
		VBox vbox = new VBox (false, 0);
		window.Add (vbox);
		vbox.Show ();
		
		// Drawing area for drawing OpenGL scene.
		ColorTriangle drawing_area = new ColorTriangle (glconfig);
		drawing_area.SetSizeRequest (200, 200);
		
		vbox.PackStart (drawing_area, true, true, 0);
		drawing_area.Show ();
		
		// Simple quit button.
		Button button = new Button ("Quit");
		button.Pressed += new EventHandler (Button_Click);
		vbox.PackStart (button, false, false, 0);
		button.Show ();
		
		// * Show window.
		window.Show ();
		
		// * Allocate colors.
		if (!is_rgba) {
			Gdk.Colormap colormap = glconfig.Colormap;
		    Console.WriteLine ("\nAllocate colors.");
		
		    // Allocate writable color cells. 
			bool[] success = new bool [NUM_COLORS];
		    int not_allocated = colormap.AllocColors (colors, NUM_COLORS, false, false, success);
		    for (int i = 0; i < NUM_COLORS; i++)
		    	Console.WriteLine ("{0}", success[i]);
		    	
		    Console.WriteLine ("Not allocated = {0}", not_allocated);
		
		    for (int i = 0; i < NUM_COLORS; i++)
		        Console.WriteLine ("colors[{0}] = [ {1}, {2}, {3}, {4} ]",  i,
																		    colors[i].Pixel,
																			colors[i].Red, 
		                													colors[i].Green, 
																			colors[i].Blue
																			);
	        Console.WriteLine ("\nQuery colors."); 
		
		    for (int i = 0; i < NUM_COLORS; i++) {
				Gdk.Color color = new Gdk.Color ();
		        color.Pixel = colors[i].Pixel;
		        colormap.QueryColor (colors[i].Pixel, ref color);
		        Console.WriteLine ("colors[{0}] = { {1}, {2}, {3}, {4} }",  i, 
																		    colors[i].Pixel,
		                													color.Red, 
																			color.Green, 
																			color.Blue);
		    }
	        Console.WriteLine (); 
		}
		
		// * Main loop.
		Gtk.Application.Run ();
		return 0;
	}


	static void Window_Delete (object obj, DeleteEventArgs args)
	{
		Gtk.Application.Quit ();
	}

	static void Button_Click (object obj, EventArgs args)
	{
		Gtk.Application.Quit ();
	}

}


