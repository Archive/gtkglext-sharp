/*
 * shapes.c:
 * shapes demo.
 *
 * written by Naofumi Yasufuku  <naofumi@users.sourceforge.net>
 */

/*
 * shapes.cs:
 * Conversion for GTK# - gtkgl-sharp.
 *
 * converted by Luciano Martorella  <mad_lux_it@users.sourceforge.net>
*/


using Gtk;
using GtkSharp;
using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using Tao.OpenGl;
using GdkGL;
using GtkGL;

namespace ShapesDemo 
{
	class 	ShapesArea : DrawingArea
	{
		static readonly double DIG_2_RAD = Math.PI / 180.0;
		static readonly double RAD_2_DIG = 180.0 / Math.PI;
		static readonly double ANIMATE_THRESHOLD = 25.0;
		
		static readonly float VIEW_SCALE_MAX = 2.0f;
		static readonly float VIEW_SCALE_MIN = 0.5f;
		
		public class ShapeProp : System.Object
		{
			public MenuItem  menu_item;
			public string 	 name;
			public ShapeProp (string n)
			{
				name = n;
				menu_item = null;
			}
		}
		private ShapeProp[] shapeArray = null;
		private int shape_list_base = 0;
		private int shape_current = 0;

		public class MaterialProp : System.Object
		{
			public float[] ambient;
			public float[] diffuse;
			public float[] specular;
			public float shininess;
			public string name;
			public MenuItem  menu_item;
			
			static public float[] Decode4V (string values)
			{
				string[] v = values.Split (null);
				if (v.Length != 4)
					throw new Exception ("Excpected 4 reals in XML value"); 
				float[] ret = new float[4];
				for (int i = 0; i < 4; i++) 
					ret[i] = (float)Double.Parse (v[i]);
				return ret;
			}
		}
			
		static public ArrayList materialList = new ArrayList ();
		private MaterialProp mat_current = null;

		static ShapesArea ()
		{
			// Read the XML file for material definition
			// No error handling
			
			// Open XML file in System.Xml.XmlDocument class
			XmlDocument materials = new XmlDocument ();
			Stream stream = File.OpenRead ("materials.xml");
			materials.Load (stream);
			stream.Close ();

			// Use System.Xml.XPath.XPathNavigator to navigate the document
			XPathNavigator nav = materials.CreateNavigator ();
			XPathNodeIterator mat_iter = nav.Select ("/materials/MaterialProp");
			while (mat_iter.MoveNext ()) {
				MaterialProp prop = new MaterialProp ();
				XPathNavigator mat_nav = mat_iter.Current;
				prop.name = mat_nav.GetAttribute ("name", "");
				XPathNodeIterator matattr_iter;

				matattr_iter = mat_nav.Select ("ambient");
				matattr_iter.MoveNext ();
				prop.ambient = MaterialProp.Decode4V(matattr_iter.Current.Value);

				matattr_iter = mat_nav.Select ("diffuse");
				matattr_iter.MoveNext ();
				prop.diffuse = MaterialProp.Decode4V(matattr_iter.Current.Value);

				matattr_iter = mat_nav.Select ("specular");
				matattr_iter.MoveNext ();
				prop.specular = MaterialProp.Decode4V(matattr_iter.Current.Value);

				matattr_iter = mat_nav.Select ("shininess");
				matattr_iter.MoveNext ();
				prop.shininess = (float)Double.Parse(matattr_iter.Current.Value);
				
				materialList.Add (prop);
			}
		}

		public ShapesArea (Config config)
		{
			m_gl = new GlWidget (this, config);
			
			AddEvents (	(int)(Gdk.EventMask.Button1MotionMask |
						Gdk.EventMask.Button2MotionMask |
						Gdk.EventMask.ButtonPressMask |
						Gdk.EventMask.ButtonReleaseMask |
						Gdk.EventMask.VisibilityNotifyMask) ); 
			Realized += new EventHandler (OnRealize);
			ConfigureEvent += new ConfigureEventHandler (OnConfigure);
			ExposeEvent += new ExposeEventHandler (OnExpose);
			
			ButtonPressEvent += new ButtonPressEventHandler (OnButtonPress); 
			ButtonReleaseEvent += new ButtonReleaseEventHandler (OnButtonRelease); 
			MotionNotifyEvent += new MotionNotifyEventHandler (OnMotionNotify); 
		
			MapEvent += new MapEventHandler (OnMap); 
			UnmapEvent += new UnmapEventHandler (OnUnmap); 
			VisibilityNotifyEvent += new VisibilityNotifyEventHandler (OnVisibilityNotify); 
			KeyPressEvent += new KeyPressEventHandler (OnKeyPress); 
		
			int i = 0;
			shapeArray = new ShapeProp [9];
			shapeArray[i++] = new ShapeProp ("Cube");
			shapeArray[i++] = new ShapeProp ("Sphere");
			shapeArray[i++] = new ShapeProp ("Cone");
			shapeArray[i++] = new ShapeProp ("Torus");
			shapeArray[i++] = new ShapeProp ("Tetrahedron");
			shapeArray[i++] = new ShapeProp ("Octahedron");
			shapeArray[i++] = new ShapeProp ("Dodecahedron");
			shapeArray[i++] = new ShapeProp ("Icosahedron");
			shapeArray[i  ] = new ShapeProp ("Teapot");
			shape_current = i;
			mat_current = (MaterialProp)materialList[0];
			
			/*
			* Popup menu.
			*/
			CreatePopupMenu ();
			ButtonPressEvent += new ButtonPressEventHandler (OnButtonPressPopupMenu);

		} 
		
		private float[] view_quat_diff = new float[4] { 0.0f, 0.0f, 0.0f, 1.0f };
		private float[] view_quat = new float[4] { 0.0f, 0.0f, 0.0f, 1.0f };
		private float view_scale = 1.0f;
		
		private bool animate = false;
		
		private void InitView ()
		{
			view_quat[0] = view_quat_diff[0] = 0.0f;
			view_quat[1] = view_quat_diff[1] = 0.0f;
			view_quat[2] = view_quat_diff[2] = 0.0f;
			view_quat[3] = view_quat_diff[3] = 1.0f;
			view_scale = 1.0f;
		}
		
		private bool realized = false;
		private GtkGL.GlWidget m_gl = null;

		private void OnRealize (object o, EventArgs args)
		{
			float[] ambient = new float[4] {0.0f, 0.0f, 0.0f, 1.0f};
			float[] diffuse = new float[4] {1.0f, 1.0f, 1.0f, 1.0f};
			float[] position = new float[4] {0.0f, 3.0f, 3.0f, 0.0f};
			
			float[] lmodel_ambient = new float[4] {0.2f, 0.2f, 0.2f, 1.0f};
			float[] local_view = new float[1] { 0.0f };
			
			/*** OpenGL BEGIN ***/
			m_gl.MakeCurrent ();
			
			Gl.glClearColor (0.5f, 0.5f, 0.8f, 1.0f);
			Gl.glClearDepth (1.0);

			Gl.glLightfv (Gl.GL_LIGHT0, Gl.GL_AMBIENT, ambient);
			Gl.glLightfv (Gl.GL_LIGHT0, Gl.GL_DIFFUSE, diffuse);
			Gl.glLightfv (Gl.GL_LIGHT0, Gl.GL_POSITION, position);
			Gl.glLightModelfv (Gl.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
			Gl.glLightModelfv (Gl.GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);

			Gl.glFrontFace (Gl.GL_CW);
			Gl.glEnable (Gl.GL_LIGHTING);
			Gl.glEnable (Gl.GL_LIGHT0);
			Gl.glEnable (Gl.GL_AUTO_NORMAL);
			Gl.glEnable (Gl.GL_NORMALIZE);
			Gl.glEnable (Gl.GL_DEPTH_TEST);
			Gl.glDepthFunc (Gl.GL_LESS);

			/* Shape display lists */
			shape_list_base = Gl.glGenLists (materialList.Count);
			int i = 0;
			
			/* Cube */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			GdkGL.Draw.Cube (true, 1.5);
			Gl.glEndList ();
			
			/* Sphere */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			GdkGL.Draw.Sphere (true, 1.0, 30, 30);
			Gl.glEndList ();
			
			/* Cone */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			Gl.glPushMatrix ();
			Gl.glTranslatef (0.0f, 0.0f, -1.0f);
			GdkGL.Draw.Cone (true, 1.0, 2.0, 30, 30);
			Gl.glPopMatrix ();
			Gl.glEndList ();
			
			/* Torus */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			GdkGL.Draw.Torus (true, 0.4, 0.8, 30, 30);
			Gl.glEndList ();
			
			/* Tetrahedron */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			Gl.glPushMatrix ();
			Gl.glScalef (1.2f, 1.2f, 1.2f);
			GdkGL.Draw.Tetrahedron (true);
			Gl.glPopMatrix ();
			Gl.glEndList ();
			
			/* Octahedron */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			Gl.glPushMatrix ();
			Gl.glScalef (1.2f, 1.2f, 1.2f);
			GdkGL.Draw.Octahedron (true);
			Gl.glPopMatrix ();
			Gl.glEndList ();
			
			/* Dodecahedron */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			Gl.glPushMatrix ();
			Gl.glScalef (0.7f, 0.7f, 0.7f);
			GdkGL.Draw.Dodecahedron (true);
			Gl.glPopMatrix ();
			Gl.glEndList ();
			
			/* Icosahedron */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			Gl.glPushMatrix ();
			Gl.glScalef (1.2f, 1.2f, 1.2f);
			GdkGL.Draw.Icosahedron (true);
			Gl.glPopMatrix ();
			Gl.glEndList ();
			
			/* Teapot */
			Gl.glNewList (shape_list_base + (i++), Gl.GL_COMPILE);
			GdkGL.Draw.Teapot (true, 1.0);
			Gl.glEndList ();
			
			/*** OpenGL END ***/
			realized = true;
			QueueResize();
		}

		private void OnConfigure (object o, ConfigureEventArgs args)
		{
			if (!realized)
				return;

			float w = Allocation.width;
			float h = Allocation.height;
			float aspect;
			
			/*** OpenGL BEGIN ***/
			m_gl.MakeCurrent ();
			
			Gl.glViewport (0, 0, (int)w, (int)h);
			
			Gl.glMatrixMode (Gl.GL_PROJECTION);
			Gl.glLoadIdentity ();
			if (w > h) {
				aspect = w / h;
				Gl.glFrustum (-aspect, aspect, -1.0f, 1.0f, 5.0f, 60.0f);
			}
			else {
				aspect = h / w;
				Gl.glFrustum (-1.0f, 1.0f, -aspect, aspect, 5.0f, 60.0f);
			}
			
			Gl.glMatrixMode (Gl.GL_MODELVIEW);
			
			/*** OpenGL END ***/
		}
				
				
		private void OnExpose (object o, ExposeEventArgs args)
		{
			if (!realized)
				return;

			float[] m = new float[4 * 4];
			
			/*** OpenGL BEGIN ***/
			m_gl.MakeCurrent ();
			
			Gl.glClear (Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
			
			Gl.glLoadIdentity ();
			
			/* View transformation. */
			Gl.glTranslatef (0.0f, 0.0f, -10.0f);
			Gl.glScalef (view_scale, view_scale, view_scale);
			Trackball.Trackball.AddQuats (view_quat_diff, view_quat, view_quat);
			Trackball.Trackball.BuildRotmatrix (m, view_quat);
			Gl.glMultMatrixf (m);
			
			/* Render shape */
			MaterialProp mat = mat_current;
			Gl.glMaterialfv (Gl.GL_FRONT, Gl.GL_AMBIENT, mat.ambient);
			Gl.glMaterialfv (Gl.GL_FRONT, Gl.GL_DIFFUSE, mat.diffuse);
			Gl.glMaterialfv (Gl.GL_FRONT, Gl.GL_SPECULAR, mat.specular);
			Gl.glMaterialf (Gl.GL_FRONT, Gl.GL_SHININESS, mat.shininess * 128.0f);
			Gl.glCallList (shape_list_base + shape_current);
			
			/* Swap buffers */
			m_gl.SwapBuffers ();
			/*** OpenGL END ***/
		}

		private float begin_x = 0.0f;
		private float begin_y = 0.0f;
		
		private float dx = 0.0f;
		private float dy = 0.0f;
		
			
		private void OnButtonPress (object o, ButtonPressEventArgs args)
		{
			if (animate) {
				if (args.Event.button == 1)
					ToggleAnimation ();
			}
			else {
				view_quat_diff[0] = 0.0f;
				view_quat_diff[1] = 0.0f;
				view_quat_diff[2] = 0.0f;
				view_quat_diff[3] = 1.0f;
			}
			
			begin_x = (float)args.Event.x;
			begin_y = (float)args.Event.y;
		}
			
		private void OnButtonRelease (object o, ButtonReleaseEventArgs args)
		{
			if (!animate) 
				if (args.Event.button == 1 && ((dx*dx + dy*dy) > ANIMATE_THRESHOLD))
					ToggleAnimation ();
			
			dx = 0.0f;
			dy = 0.0f;
		}
		
		
		private void OnMotionNotify (object o, MotionNotifyEventArgs args)
		{
			float w = Allocation.width;
			float h = Allocation.height;
			float x = (float)args.Event.x;
			float y = (float)args.Event.y;
			bool redraw = false;
			
			/* Rotation. */
			if ((args.Event.state & (uint)Gdk.ModifierType.Button1Mask) != 0) {
				Trackball.Trackball.SimulateTrackball (view_quat_diff,
						 (2.0f * begin_x - w) / w,
						 (h - 2.0f * begin_y) / h,
						 (2.0f * x - w) / w,
						 (h - 2.0f * y) / h);
				
				dx = x - begin_x;
				dy = y - begin_y;
		
				redraw = true;
			}
			
			/* Scaling. */
			if ((args.Event.state & (uint)Gdk.ModifierType.Button2Mask) != 0) {
				view_scale = view_scale * (1.0f + (y - begin_y) / h);
				if (view_scale > VIEW_SCALE_MAX)
					view_scale = VIEW_SCALE_MAX;
				else if (view_scale < VIEW_SCALE_MIN)
					view_scale = VIEW_SCALE_MIN;
				
				redraw = true;
			}
			
			begin_x = x;
			begin_y = y;
			
			if (redraw && !animate)
			    GdkWindow.InvalidateRect (Allocation, false);
		}
			
		private void OnKeyPress (object o, KeyPressEventArgs args)
		{
			if (args.Event.keyval == (uint)Gdk.Key.Escape) 
				Gtk.Application.Quit ();
		}
			
		/* Toggle animation.*/
		private void ToggleAnimation ()
		{
			animate = !animate;
			if (animate)
			    IdleAdd ();
			else {
			    IdleRemove ();
			    view_quat_diff[0] = 0.0f;
			    view_quat_diff[1] = 0.0f;
			    view_quat_diff[2] = 0.0f;
			    view_quat_diff[3] = 1.0f;
			    GdkWindow.InvalidateRect (Allocation, false);
			}
		}
			
		[IdlePriorityAttribute (IdlePriorityEnum.Redraw)]
		private void OnIdle (object o, EventArgs args)
		{
			/* Invalidate the whole window. */
		    GdkWindow.InvalidateRect (Allocation, false);
			
			/* Update synchronously. */
		    GdkWindow.ProcessUpdates (false);
		}
		
		private bool IdleInstalled = false;
		
		private void IdleAdd ()
		{
			if (!IdleInstalled) 
				Gtk.Application.Idle += new EventHandler (OnIdle);
			IdleInstalled = true;
		}

		private void IdleRemove ()
		{
			if (IdleInstalled) 
				Gtk.Application.Idle -= new EventHandler (OnIdle);
			IdleInstalled = false;
		}

		private void OnMap (object o, MapEventArgs args)
		{
			if (animate)	
				IdleAdd ();
		}
		
		private void OnUnmap (object o, UnmapEventArgs args)
		{
			IdleRemove ();
		}

		private void OnVisibilityNotify (object o, VisibilityNotifyEventArgs args)
		{
			if (animate) 
			    if (args.Event.state == Gdk.VisibilityState.FullyObscured)
					IdleRemove ();
			    else
					IdleAdd ();
		}
		
		private Menu menu = null;
		
		/* Creates the popup menu.*/
		private void CreatePopupMenu ()
		{
			/*
			* Shapes submenu.
			*/
			
			Menu shapes_menu = new Menu ();
			MenuShell shell = new MenuShell (shapes_menu.Raw);
			
			for (int i = 0; i < shapeArray.Length; i++) {
				MenuItem menu_item = new MenuItem (shapeArray[i].name);
				shell.Append (menu_item);
				menu_item.Activated += new EventHandler (OnChangeShape);
				shapeArray[i].menu_item = menu_item;
				menu_item.Show ();
			}
			
			/*
			* Materials submenu.
			*/
			
			Menu materials_menu = new Menu ();
			shell = new MenuShell (materials_menu.Raw);
			
			for (int i = 0; i < materialList.Count; i++) {
				MenuItem menu_item = new MenuItem (((MaterialProp)materialList[i]).name);
				shell.Append (menu_item);
				menu_item.Activated += new EventHandler (OnChangeMaterial);
				((MaterialProp)materialList[i]).menu_item = menu_item;
				menu_item.Show ();
			}

			/* 
			* Root popup menu.
			*/
			
			menu = new Menu ();
			shell = new MenuShell (menu.Raw);
			
			/* Shapes */
			MenuItem item = new MenuItem ("Shapes");
			item.Submenu = shapes_menu;
			shell.Append (item);
			item.Show ();
			
			/* Materials */
			item = new MenuItem ("Materials");
			item.Submenu = materials_menu;
			shell.Append (item);
			item.Show ();
	
			/* Quit */
			item = new MenuItem ("Quit");
			shell.Append (item);
			item.Activated += new EventHandler (MainApp.OnQuit);
			item.Show ();
		}
		
		private void OnChangeShape (object o, EventArgs args)
		{
			for (int i = 0; i < shapeArray.Length; i++)
				if (o == shapeArray[i].menu_item) {
					shape_current = i;
					return;
				}
			InitView ();
		}

		private void OnChangeMaterial (object o, EventArgs args)
		{
			for (int i = 0; i < materialList.Count; i++)
				if (o == ((MaterialProp)materialList[i]).menu_item) {
					mat_current = (MaterialProp)materialList[i];
					return;
				}
			InitView ();
		}
		
		private void OnButtonPressPopupMenu (object o, ButtonPressEventArgs args)
		{
		    /* Popup menu. */
			if (args.Event.button == 3)	
				menu.Popup (null, null, null, IntPtr.Zero, 0, Gtk.Global.CurrentEventTime);
		}		
	} 


	class MainApp {
		
		public static void OnQuit (object o, EventArgs args)
		{
			Gtk.Application.Quit ();
		}
		public static void OnDelete (object o, DeleteEventArgs args)
		{
			Gtk.Application.Quit ();
		}
	
		static public int Main (string[] args)
		{
			Gtk.Application.Init (ref args);
			GtkGL.Application.Init (ref args);

			/*
			* Query OpenGL extension version.
			*/
			int major, minor;
			Query.Version (out major, out minor);
			Console.WriteLine ("\nOpenGL extension version - {0}.{1}", major, minor);

			// * Configure OpenGL-capable visual.
			// Try double-buffered visual 
			GdkGL.Config glconfig = new GdkGL.Config (GdkGL.ConfigMode.Rgb | GdkGL.ConfigMode.Double);
			if (glconfig == null) {
	      		Console.WriteLine ("*** Cannot find the double-buffered visual.\n*** Trying single-buffered visual.");
				glconfig = new GdkGL.Config (GdkGL.ConfigMode.Rgb);
				if (glconfig == null) {
			          Console.WriteLine ("*** Cannot find any OpenGL-capable visual.");
			          return 1;
				} 
			}
			
			GlUtilities.WriteOutConfig (glconfig);
			
			// Top-level window.
			Window window = new Window (WindowType.Toplevel);
			window.Title = "shapes";

			// Get automatically redrawn if any of their children changed allocation. 
			window.ReallocateRedraws = true;
			window.DeleteEvent += new DeleteEventHandler (OnDelete);

			// VBox.
			VBox vbox = new VBox (false, 0);
			window.Add (vbox);
			vbox.Show ();
			
			// Drawing area for drawing OpenGL scene.
			ShapesArea drawing_area = new ShapesArea (glconfig);
			drawing_area.SetSizeRequest (300, 300);

			vbox.PackStart (drawing_area, true, true, 0);
			drawing_area.Show ();

			// Simple quit button.
			Button button = new Button ("Quit");
			button.Clicked += new EventHandler (OnQuit);
			vbox.PackStart (button, false, false, 0);
			button.Show ();
			
			// Show window.
			window.Show ();		
			
			//
			Gtk.Application.Run ();
			return 0;
		} 
		
	}

}


