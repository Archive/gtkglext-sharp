DIRS = gdkgl gtkgl examples
MCS = mcs

all:
	for i in $(DIRS); do				\
		MCS="$(MCS)" $(MAKE) -C $$i || exit 1;\
	done;

gen-clean:
	for i in $(DIRS); do		\
		$(MAKE) -C $$i clean || exit 1;		\
	done;

#maintainer-clean: distclean
#	rm -f aclocal.m4 config.guess config.h.in config.sub
#	rm -f configure install-sh ltmain.sh missing
#	rm -f mkinstalldirs stamp-h glue/Makefile.in

install: 
	for i in $(DIRS); do				\
		$(MAKE) -C $$i install || exit 1;	\
	done
